package ru.baboytsev.exam.service;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.easymock.EasyMockUnitils;
import org.unitils.easymock.annotation.Mock;
import ru.baboytsev.exam.model.FileInfo;
import ru.baboytsev.exam.model.UploadedFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@RunWith(UnitilsJUnit4TestClassRunner.class)
public class FileServiceImplTest {
    private static final String FILE_NAME = "blue-bullet-big.png";
    private static final String PREVIEW_FILE_NAME = "blue-bullet.png";

    private String expectedBase64Preview;

    private FileServiceImpl fileService;

    @Mock
    private PreviewCreator previewCreator;


    @Before
    public void setUp() throws Exception {
        expectedBase64Preview = Base64.getEncoder().encodeToString(getPreviewBytes());

        fileService = new FileServiceImpl();
        fileService.setPreviewCreator(previewCreator);
    }

    @Test
    public void testUploadFiles() throws Exception {
        //prepare
        EasyMock.expect(previewCreator.cretePreview(EasyMock.anyObject())).andReturn(readImage());
        List<FileInfo> fileInfoList = getFileInfoList();
        EasyMockUnitils.replay();
        //act
        List<UploadedFile> result = fileService.uploadFiles(fileInfoList);
        //assert
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        UploadedFile uploadedFile = result.get(0);
        Assert.assertNotNull(uploadedFile);
        Assert.assertEquals(FILE_NAME, uploadedFile.getName());
        Assert.assertNotNull(uploadedFile.getUri());
        Assert.assertEquals(expectedBase64Preview, uploadedFile.getPreviewBase64());

        System.out.println(expectedBase64Preview);
    }

    @Test
    public void testUploadFiles_basePath() throws Exception {
        //prepare
        String basePath = FileServiceImplTest.class.getResource("/").getPath();
        fileService.setBasePath(basePath);
        EasyMock.expect(previewCreator.cretePreview(EasyMock.anyObject())).andReturn(readImage());
        List<FileInfo> fileInfoList = getFileInfoList();
        EasyMockUnitils.replay();
        //act
        List<UploadedFile> result = fileService.uploadFiles(fileInfoList);
        //assert
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        UploadedFile uploadedFile = result.get(0);
        Assert.assertNotNull(uploadedFile);
        Assert.assertEquals(FILE_NAME, uploadedFile.getName());
        Assert.assertNotNull(uploadedFile.getUri());
        Assert.assertTrue(uploadedFile.getUri().startsWith(basePath));
        Assert.assertEquals(expectedBase64Preview, uploadedFile.getPreviewBase64());
    }

    private List<FileInfo> getFileInfoList() {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setFileName(FILE_NAME);
        fileInfo.setContent(FileServiceImplTest.class.getResourceAsStream("/" + FILE_NAME));
        return Collections.singletonList(fileInfo);
    }

    private BufferedImage readImage() throws Exception {
        File imageFile = new File(FileServiceImplTest.class.getResource("/" + PREVIEW_FILE_NAME).toURI());
        return ImageIO.read(imageFile);
    }

    private byte[] getPreviewBytes() throws Exception {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            BufferedImage previewImage = ImageIO.read(new File(FileServiceImplTest.class.getResource("/" + PREVIEW_FILE_NAME).toURI()));
            ImageIO.write(previewImage, "png", outputStream);
            return outputStream.toByteArray();
        }
    }
}
