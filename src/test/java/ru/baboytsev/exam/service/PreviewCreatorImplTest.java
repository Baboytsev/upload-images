package ru.baboytsev.exam.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.RenderedImage;
import java.io.File;

public class PreviewCreatorImplTest {
    private static final String FILE_NAME = "blue-bullet-big.png";
    private static final String PREVIEW_FILE_NAME = "blue-bullet.png";

    private PreviewCreatorImpl previewCreator;

    @Before
    public void setUp() {
        previewCreator = new PreviewCreatorImpl();
    }

    @Test
    public void testCretePreview() throws Exception {
        //prepare
        File originalFile = new File(PreviewCreatorImplTest.class.getResource("/" + FILE_NAME).toURI());
        //act
        RenderedImage result = previewCreator.cretePreview(originalFile);
        //assert
        Assert.assertNotNull(result);
        Assert.assertEquals(100, result.getHeight());
        Assert.assertEquals(100, result.getWidth());
    }

}
