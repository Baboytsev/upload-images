package ru.baboytsev.exam.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class UtilsTest {

    @Test
    public void testExtractImageName() {
        //prepare
        String expectedImageName = "0_a9e8c_fefaa1d2_XL-640x400.jpg";
        String url = "https://bipbap.ru/wp-content/uploads/2017/10/" + expectedImageName;
        //act
        Optional<String> result = Utils.extractImageName(url);
        //assert
        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(expectedImageName, result.get());
    }

    @Test
    public void testExtractImageName_notImage() {
        //prepare
        String url = "https://bipbap.ru/wp-content/uploads/2017/10/file.txt";
        //act
        Optional<String> result = Utils.extractImageName(url);
        //assert
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void testExtractImageName_notFile() {
        //prepare
        String url = "https://bipbap.ru";
        //act
        Optional<String> result = Utils.extractImageName(url);
        //assert
        Assert.assertFalse(result.isPresent());
    }
}
