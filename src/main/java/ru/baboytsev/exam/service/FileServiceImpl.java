package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exceptions.FileServiceException;
import ru.baboytsev.exam.exceptions.PreviewCreationException;
import ru.baboytsev.exam.model.FileInfo;
import ru.baboytsev.exam.model.UploadedFile;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

public class FileServiceImpl implements FileService {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_BASE_PATH = FileServiceImpl.class.getResource("/").getPath();

    private PreviewCreator previewCreator;
    private String basePath;

    public void setPreviewCreator(PreviewCreator previewCreator) {
        this.previewCreator = previewCreator;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public List<UploadedFile> uploadFiles(List<FileInfo> inputFileList) throws FileServiceException {
        List<UploadedFile> uploadedFiles = new ArrayList<>();
        for (FileInfo fileInfo : inputFileList) {
            String uri = generateUri(fileInfo.getFileName());
            File file = saveFile(uri, fileInfo.getContent());
            UploadedFile uploadedFile = new UploadedFile();
            uploadedFile.setName(fileInfo.getFileName());
            uploadedFile.setUri(uri);
            uploadedFile.setPreviewBase64(createBase64Preview(file));
            uploadedFiles.add(uploadedFile);
        }
        return uploadedFiles;
    }

    private String generateUri(String fileName) {
        String currentDate = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        String[] dateAndTimeParts = currentDate.split(" ");
        String[] dateParts = dateAndTimeParts[0].split("-");
        String[] timeParts = dateAndTimeParts[1].split(":");
        if (basePath == null) {
            basePath = DEFAULT_BASE_PATH;
        }
        return basePath + "/" + dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2]
                + "/" + timeParts[0] + "/" + timeParts[1] + "/" + timeParts[2] + "/" + fileName;
    }

    private File saveFile(String uri, InputStream content) throws FileServiceException {
        try {
            Path filePath = Paths.get(uri);
            if (Files.notExists(filePath)) {
                Files.createDirectories(filePath);
            }
            Files.copy(content, filePath, StandardCopyOption.REPLACE_EXISTING);
            return filePath.toFile();
        } catch (IOException e) {
            throw new FileServiceException("Can not save file", e);
        }
    }

    private String createBase64Preview(File image) throws FileServiceException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            RenderedImage previewImage = previewCreator.cretePreview(image);
            ImageIO.write(previewImage, "png", outputStream);
            return Base64.getEncoder().encodeToString(outputStream.toByteArray());
        } catch (IOException | PreviewCreationException e) {
            throw new FileServiceException("Can not create base64 png preview.", e);
        }
    }

}
