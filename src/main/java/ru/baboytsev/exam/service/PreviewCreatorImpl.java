package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exceptions.PreviewCreationException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

public class PreviewCreatorImpl implements PreviewCreator {


    @Override
    public RenderedImage cretePreview(File original) throws PreviewCreationException {
        try {
            return toBufferedImage(ImageIO.read(original).getScaledInstance(100, 100, BufferedImage.SCALE_SMOOTH));
        } catch (IOException e) {
            throw new PreviewCreationException("Can not crete preview", e);
        }
    }

    private BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        return bimage;
    }
}
