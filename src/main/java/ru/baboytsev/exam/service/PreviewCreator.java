package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exceptions.PreviewCreationException;

import java.awt.image.RenderedImage;
import java.io.File;

public interface PreviewCreator {

    /**
     * Create preview for given image file.
     *
     * @param original - original image file.
     * @return - preview for image
     * @throws PreviewCreationException - if can not read original file or create preview.
     */
    RenderedImage cretePreview(File original) throws PreviewCreationException;
}
