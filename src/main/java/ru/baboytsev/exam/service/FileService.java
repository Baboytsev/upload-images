package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exceptions.FileServiceException;
import ru.baboytsev.exam.model.FileInfo;
import ru.baboytsev.exam.model.UploadedFile;

import java.util.List;

public interface FileService {


    /**
     * Uploading files, creating preview and saving.
     *
     * @param inputFileList - list of file for saving
     * @return - saved files info
     * @throws FileServiceException - if can not save file or create preview.
     */
    List<UploadedFile> uploadFiles(List<FileInfo> inputFileList) throws FileServiceException;

}
