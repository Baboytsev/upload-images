package ru.baboytsev.exam.exceptions;

public class PreviewCreationException extends Exception {

    public PreviewCreationException() {
    }

    public PreviewCreationException(String message) {
        super(message);
    }

    public PreviewCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreviewCreationException(Throwable cause) {
        super(cause);
    }

    public PreviewCreationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
