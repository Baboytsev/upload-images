package ru.baboytsev.exam.exceptions;

public class UploadFileServiceException extends Exception {

    public UploadFileServiceException() {
    }

    public UploadFileServiceException(String message) {
        super(message);
    }

    public UploadFileServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadFileServiceException(Throwable cause) {
        super(cause);
    }

    public UploadFileServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
