package ru.baboytsev.exam.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class UploadFileRestConfig extends ResourceConfig {

    public UploadFileRestConfig() {
        register(RequestContextFilter.class);
        register(UploadFileRestFacade.class);
        register(JacksonFeature.class);

    }
}
