package ru.baboytsev.exam.rest;

import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import ru.baboytsev.exam.exceptions.FileServiceException;
import ru.baboytsev.exam.exceptions.UploadFileServiceException;
import ru.baboytsev.exam.model.FileDTO;
import ru.baboytsev.exam.model.FileInfo;
import ru.baboytsev.exam.model.UploadedFile;
import ru.baboytsev.exam.service.FileService;
import ru.baboytsev.exam.util.Utils;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

@Component
@Path("/")
public class UploadFileRestFacade {
    private static final String ERROR_CAN_NOT_UPLOAD = "Can not upload files.";

    private FileService fileService;

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA_VALUE)
    @Produces(MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<UploadedFile> uploadFiles(FormDataMultiPart multiPart) throws UploadFileServiceException {
        List<FormDataBodyPart> files = multiPart.getFields("files");
        if (CollectionUtils.isEmpty(files)) {
            return new ArrayList<>();
        }
        try {
            List<FileInfo> fileInfoList = new ArrayList<>();
            for (FormDataBodyPart formDataBodyPart : files) {
                fileInfoList.add(convert(formDataBodyPart));
            }
            return fileService.uploadFiles(fileInfoList);
        } catch (FileServiceException e) {
            throw new UploadFileServiceException(ERROR_CAN_NOT_UPLOAD, e);
        }
    }

    @POST
    @Path("/uploadBase64")
    @Consumes(MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Produces(MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<UploadedFile> uploadBase64Files(List<FileDTO> fileDTOList) throws UploadFileServiceException {
        if (CollectionUtils.isEmpty(fileDTOList)) {
            return Collections.emptyList();
        }
        try {
            List<FileInfo> fileInfoList = new ArrayList<>();
            for (FileDTO fileDTO : fileDTOList) {
                fileInfoList.add(convert(fileDTO));
            }
            return fileService.uploadFiles(fileInfoList);
        } catch (FileServiceException e) {
            throw new UploadFileServiceException(ERROR_CAN_NOT_UPLOAD, e);
        }
    }

    @POST
    @Path("/uploadUrl")
    @Produces(MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UploadedFile uploadFile(@FormDataParam("url") String url) throws UploadFileServiceException {
        Optional<String> imageName = Utils.extractImageName(url);
        if (imageName.isPresent()) {
            try {
                FileInfo fileInfo = new FileInfo();
                fileInfo.setFileName(imageName.get());
                InputStream content = getContentFromUrl(url);
                fileInfo.setContent(content);
                return fileService.uploadFiles(Collections.singletonList(fileInfo)).iterator().next();
            } catch (FileServiceException e) {
                throw new UploadFileServiceException(ERROR_CAN_NOT_UPLOAD, e);
            }
        } else {
            throw new UploadFileServiceException("Url '" + url + "' is not image.");
        }
    }

    private FileInfo convert(FormDataBodyPart formDataBodyPart) throws UploadFileServiceException {
        try {
            BodyPartEntity bodyPartEntity = (BodyPartEntity) formDataBodyPart.getEntity();
            String fileName = formDataBodyPart.getContentDisposition().getFileName();
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(Utils.restoreUTF8FileName(fileName));
            fileInfo.setContent(bodyPartEntity.getInputStream());
            return fileInfo;
        } catch (Exception e) {
            throw new UploadFileServiceException("Exception while reading file from multipart.", e);
        }
    }

    private FileInfo convert(FileDTO fileDTO) throws UploadFileServiceException {
        try {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(fileDTO.getName());
            fileInfo.setContent(new ByteArrayInputStream(Base64.getDecoder().decode(fileDTO.getBase64Content())));
            return fileInfo;
        } catch (IllegalArgumentException e) {
            throw new UploadFileServiceException("Exception while decoding base64 file content.", e);
        }
    }

    private InputStream getContentFromUrl(String url) throws UploadFileServiceException {
        try {
            return new BufferedInputStream(new URL(url).openStream());
        } catch (IOException e) {
            throw new UploadFileServiceException("Can not get file content from url '" + url + "'", e);
        }
    }


}
