package ru.baboytsev.exam.util;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final Pattern IMAGE_EXTENSION_PATTERN = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");

    public static Optional<String> extractImageName(String url) {
        String mayBeImageName = url.substring(url.lastIndexOf("/") + 1);
        Matcher matcher = IMAGE_EXTENSION_PATTERN.matcher(mayBeImageName);
        if (matcher.matches()) {
            return Optional.of(mayBeImageName);
        } else return Optional.empty();
    }

    public static String restoreUTF8FileName(String isoFileName) {
        String result;
        byte fileNameISOBytes[] = isoFileName.getBytes(StandardCharsets.ISO_8859_1);
        String fileNameUTF8 = new String(fileNameISOBytes, StandardCharsets.UTF_8);
        if (isoFileName.length() != fileNameUTF8.length()) {
            result = new String(isoFileName.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        } else {
            result = isoFileName;
        }
        return result;
    }
}
