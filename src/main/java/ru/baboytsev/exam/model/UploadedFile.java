package ru.baboytsev.exam.model;

public class UploadedFile {

    private String uri;
    private String name;
    private String previewBase64;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreviewBase64() {
        return previewBase64;
    }

    public void setPreviewBase64(String previewBase64) {
        this.previewBase64 = previewBase64;
    }
}
